
public class CoreJavaBrushUp3 {

	public static void main(String[] args) {
		// string is an object
		String s1 = "Andrei Negura Academy";
		String s2 = "bla bla bla";

		String s3 = new String("welcome");
		String s4 = new String("welcome"); // aici se aloca un nou spatiu de memorie (se creeaza un obiect nou) de
											// fiecare data cu "new"

		s1.split(" ");
		String[] splittedString1 = s1.split(" ");
		System.out.println(splittedString1[0]);
		System.out.println(splittedString1[1]);
		System.out.println(splittedString1[2]);
		System.out.println();

		String[] splittedString2 = s1.split("Negura");
		System.out.println(splittedString2[0]);
		System.out.println(splittedString2[1]);
		System.out.println(splittedString2[1].trim());
//		for (int i = s1.length()-1; i > 0; i--) {       // for printing in the reverse order
		for (int i = 0; i < s1.length(); i++) {
			System.out.println(s1.charAt(i));
		}
	}
}
