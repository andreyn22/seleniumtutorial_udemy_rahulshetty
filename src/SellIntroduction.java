import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SellIntroduction {

	public static void main(String[] args) {
//		Invoking Browser
//		Chrome _ChromeDriver extend -> Methods close get
//		Firefox -FirefoxDriver -> methods close get
//		Safari SafariDriver -> methods close get
//		WebDriver close get
//		WebDriver methods + class methods

//		Chrome Launch
		System.setProperty("webdriver.chrome.driver", "C:/Users/anegura/drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

//		Firefox Launch
		System.setProperty("webdriver.gecko.driver", "C:/Users/anegura/drivers/geckodriver.exe");
		WebDriver driver1= new FirefoxDriver();

//		Edge Launch
		System.setProperty("webdriver.edge.driver", "C:/Users/anegura/drivers/msedgedriver.exe");
		WebDriver driver2 = new EdgeDriver();

		driver.get("https://rahulshettyacademy.com");
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
//		driver.close();
		driver.quit();
	}
}
