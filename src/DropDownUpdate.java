import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class DropDownUpdate {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:/Users/anegura/drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		Assert.assertFalse(driver.findElement(By.cssSelector("input[id='ctl00_mainContent_chk_SeniorCitizenDiscount']"))
				.isSelected());
		driver.findElement(By.cssSelector("input[id='ctl00_mainContent_chk_SeniorCitizenDiscount']")).click();
		System.out.println(driver.findElement(By.cssSelector("input[id='ctl00_mainContent_chk_SeniorCitizenDiscount']"))
				.isSelected());
		Assert.assertTrue(driver.findElement(By.cssSelector("input[id='ctl00_mainContent_chk_SeniorCitizenDiscount']"))
				.isSelected());

//		System.out.println(driver.findElement(By.name("ctl00$mainContent$view_date2")).isEnabled());
		System.out.println(driver.findElement(By.id("Div1")).getAttribute("style"));
		driver.findElement(By.id("ctl00_mainContent_rbtnl_Trip_1")).click();
		System.out.println(driver.findElement(By.id("Div1")).getAttribute("style"));

		if (driver.findElement(By.id("Div1")).getAttribute("style").contains("1")) {
			System.out.println("its enabled");
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);
		}

		// Count the number of checkboxes
		driver.findElement(By.id("divpaxinfo")).click();
		Thread.sleep(2000);
		System.out.println("There are a number of "
				+ driver.findElements(By.cssSelector("input[type='checkbox']")).size() + " checkboxes");

//		  driver.findElement(By.id("hrefIncAdt")).click();
//		  driver.findElement(By.id("hrefIncAdt")).click();
//		  driver.findElement(By.id("hrefIncAdt")).click();
//		  driver.findElement(By.id("hrefIncAdt")).click(); //1 default + 4 = 5 adults

//		int i = 1;
//		while (i < 5) {
//			driver.findElement(By.id("hrefIncAdt")).click(); // 1 default + 4 times
//			i++;
//		}

		for (int i = 1; i < 5; i++) {
			driver.findElement(By.id("hrefIncAdt")).click(); // 1 default + 4 times
		}
		driver.findElement(By.id("btnclosepaxoption")).click();
		Assert.assertEquals(driver.findElement(By.id("divpaxinfo")).getText(), "5 Adult");
		System.out.println("It was selected a number of " + driver.findElement(By.id("divpaxinfo")).getText() + "s");
		driver.close();
	}
}
