
public class MethodsDemo1 {

	public static void main(String[] args) {

		MethodsDemo1 d = new MethodsDemo1();
		int age = d.getData2();
		String name = d.getData3();
		System.out.println(name);
		MethodsDemo2 d1 = new MethodsDemo2();
		d1.getUserData();

	}

	public void getData1() {
	}

	public int getData2() {
		System.out.println("Integer");
		return 32;
	}

	public String getData3() {
		System.out.println("String");
		return "bla bla";

	}
}
