import java.util.ArrayList;

public class CoreJavaBrushUp1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int myNum = 5;
		String website = "Andrei Negura Academy";
		char letter = 'r';
		double dec = 5.9;
		boolean myCard = true;

		System.out.println(myNum + " is the value stored in the MyNum variable");
		System.out.println(website);

		// Arrays
		System.out.println();
		int[] arr1 = new int[5];
		arr1[0] = 1;
		arr1[1] = 2;
		arr1[2] = 4;
		arr1[3] = 5;
		arr1[4] = 6;

		int[] arr2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15 };
		System.out.println(arr1[1] + " is the value for the element with index1 of arr1");
		System.out.println(arr2[2] + " is the value for the element with index2 of arr2");

		// for loop
		System.out.println();
		System.out.println("Bellow the array with length <5: ");
		for (int i = 0; i < 5; i++) {
			System.out.println(arr2[i]);
		}

		System.out.println();
		System.out.println("Bellow the array with his entire length: ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.println(arr2[i]);
		}

		System.out.println();
		System.out.println("Iteration of strings with standard for loop:");
		String[] name = { "rahul", "Shetty", "selenium" };
		for (int i = 0; i < name.length; i++) {
			System.out.println(name[i]);
		}

		System.out.println();
		System.out.println("Enhanced for loop:");
		for (String s : name) {
			System.out.println(s);
		}

	}
}
