import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment {

	public static void main(String[] args) throws InterruptedException {

		String text1 = "Rahul";
		String text2 = "Andrei";

		System.setProperty("webdriver.chrome.driver", "C:/Users/anegura/drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		Thread.sleep(2000);
		driver.findElement(By.id("name")).sendKeys(text1);
		Thread.sleep(2000);
//		driver.findElement(By.cssSelector("[id='alertbtn']")).click();
		WebElement alertButton = driver.findElement(By.cssSelector("[id='alertbtn']"));
		HighLighter.highLight(alertButton, driver);
		alertButton.click();
		Thread.sleep(2000);

		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().accept();
		Thread.sleep(2000);

//		driver.findElement(By.id("confirmbtn")).click();
		driver.findElement(By.id("name")).sendKeys(text2);
		WebElement confirmButton = driver.findElement(By.id("confirmbtn"));
		HighLighter.highLight(confirmButton, driver);
		confirmButton.click();
		Thread.sleep(2000);

		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().dismiss();
		Thread.sleep(2000);

		driver.close();
	}

}
