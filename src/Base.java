import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {

	public static void main(String[] args) throws InterruptedException {
//		System.setProperty("webdriver.chrome.driver", "C:/Users/anegura/drivers/chromedriver.exe");
//		WebDriverManager.chromedriver().browserVersion("106.0.5249.62").setup();
		WebDriverManager.chromedriver().driverVersion("106.0.5249.62").setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		WebDriverWait w = new WebDriverWait(driver, Duration.ofSeconds(5)); // declaring WebDriverWait globally
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); // implicit wait

		String[] itemsNeeded = { "Cucumber", "Brocolli", "Beetroot", "Brinjal" };

		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		Thread.sleep(3000);
		addItems(driver, itemsNeeded);
		driver.findElement(By.cssSelector("img[alt='Cart']")).click();
		driver.findElement(By.xpath("//button[contains(text(),'PROCEED TO CHECKOUT')]")).click();
		w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input.promoCode"))); // explicit wait
		driver.findElement(By.cssSelector("input.promoCode")).sendKeys("rahulshettyacademy");
		driver.findElement(By.cssSelector("button.promoBtn")).click();

		w.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.promoInfo"))); // explicit wait

		System.out.println(driver.findElement(By.cssSelector("span.promoInfo")).getText());

	}

	public static void addItems(WebDriver driver, String[] itemsNeeded) throws InterruptedException {
		int j = 0;

		List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name"));
		for (int i = 0; i < products.size(); i++) {
			String[] name = products.get(i).getText().split(("-"));
			String formattedName = name[0].trim();

//			 check whether name you extracted is present in array or not
//			 convert array into array list for easy search
//			 check wheter name you extracted is present in arraylist or not

			List itemsNeededList = Arrays.asList(itemsNeeded);

			// convert array into array list for easy search
			if (itemsNeededList.contains(formattedName)) {
				j++;
				driver.findElements(By.xpath("//div[@class='product-action']")).get(i).click();
//				driver.findElement(By.xpath("//button[text()='ADD TO CART']")).click();
				if (j == itemsNeeded.length) {
					break;
				}

			}
		}
		Thread.sleep(5000);
		driver.close();
	}

}
