import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ActionsDemo {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().driverVersion("106.0.5249.62").setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		WebDriverWait w = new WebDriverWait(driver, Duration.ofSeconds(10));
		driver.get("https://amazon.com");
		Actions a = new Actions(driver);
		WebElement move = driver.findElement(By.xpath(".//*[@id='nav-link-accountList']"));

//		w.until(ExpectedConditions.visibilityOfElementLocated(By.id("twotabsearchtextbox"))); 
		a.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).click().keyDown(Keys.SHIFT).sendKeys("Hello")
				.doubleClick().build().perform();
		// Moves to specific element

//		a.moveToElement(move).build().perform();
		a.moveToElement(move).contextClick().build().perform();
		a.moveToElement(move).clickAndHold();
		Thread.sleep(5000);
		driver.close();
	}
}
